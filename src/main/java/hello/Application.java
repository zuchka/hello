package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@CrossOrigin
	@GetMapping(value = "/languages", produces = "application/json")
	public String languages(){
		String languages = "[{\"Language\":\"jQuery\",\"ID\":\"1\"},{\"Language\":\"C#\",\"ID\":\"2\"},{\"Language\":\"PHP\",\"ID\":\"3\"},{\"Language\":\"Java\",\"ID\":\"4\"},{\"Language\":\"Python\",\"ID\":\"5\"},{\"Language\":\"Perl\",\"ID\":\"6\"},{\"Language\":\"C++\",\"ID\":\"7\"},{\"Language\":\"ASP\",\"ID\":\"8\"},{\"Language\":\"Ruby\",\"ID\":\"9\"}]";
		return languages;
	}

}
